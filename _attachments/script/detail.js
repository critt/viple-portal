
$(function() {   
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    var path = unescape(document.location.pathname).split('/'),
        design = path[3],
        db = $.couch.db(path[1]);
    function drawItems() {
        var lookupID = location.search.split('session=')[1]
        var returnObj = db.view(design + "/detail", {
            key : lookupID,
            descending : "true",
            limit : 1,
            update_seq : true,
            success : function(data) {
                var coords = data.rows[0].value.coords;
                if(coords) drawVisualization(coords);
                setupChanges(data.update_seq);
                var them = $.mustache($("#recent-messages").html(), {
                    items : data.rows.map(function(r) {return r.value;})
                });
                $("#content").append(them);
            }
        });
        
    };
    drawItems();
    var changesRunning = false;
    function setupChanges(since) {
        if (!changesRunning) {
            var changeHandler = db.changes(since);
            changesRunning = true;
            //changeHandler.onChange(drawItems);
        }
    }

    $.couchProfile.templates.profileReady = $("#new-message").html();
    $("#account").couchLogin();


    var data = null;
    var graph = null;

    function custom(x, y) {
        return (Math.sin(x / 50) * Math.cos(y / 50) * 50 + 50);
    }

    // Called when the Visualization API is loaded.
    function drawVisualization(coords) {
        // Create and populate a data table.
        var data = new vis.DataSet();
        data.add(coords);

        // specify options
        var options = {
            width: '100%',
            height: '600px',
            style: 'line',
            tooltip: 'true',
            showPerspective: true,
            showGrid: true,
            showShadow: false,
            keepAspectRatio: true,
            verticalRatio: 0.5,
            xMax: 12,
            xMin: -12,
            yMax: 12,
            yMin: -12,
            zMax: 12,
            zMin: -12
        };

        // create a graph3d
        var container = document.getElementById('mygraph');
        graph3d = new vis.Graph3d(container, data, options);
    }
 });