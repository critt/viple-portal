
function(doc) {
  t = new Date(doc.time);
  if (doc.sessionID) {
      emit(doc.sessionID, {
          time: t,
          os: doc.OS,
          devicetype: doc.deviceType,
          graphicsdevicename: doc.graphicsDeviceName,
          graphicsdeviceversion: doc.graphicsDeviceVersion,
          processor: doc.processor,
          processorcount: doc.processorCount,
          systemmemory: doc.systemMemory,
          ipaddress: doc.ipAddress,
          sessionid: doc.sessionID,
          clientid: doc.clientID,
          latitude: doc.latitude,
          longitude: doc.longitude,
          location: doc.city + ", " + doc.state + ", " + doc.country,
          coords: doc.coords
      });
  }
};

