
function(doc) {
  t = new Date(doc.time);
  if (t) {
      emit(t, {
          time: t,
          os: doc.OS,
          devicetype: doc.deviceType,
          ipaddress: doc.ipAddress,
          sessionid: doc.sessionID,
          clientid: doc.clientID,
          location: doc.city + ", " + doc.state + ", " + doc.country
      });
  }
};

