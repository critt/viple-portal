# README #

ASU Viple Analytics Portal.

### This web application displays a live feed of Unity Simulator sessions. ###

* This is a couchapp, and is intended for use in a CouchDB database
* This is a brief guide about setting up a CouchDB instance, installing this application on it, and setting up the Unity Simulator account in the database

### CouchDB Setup ###

* Install CouchDB on your server
* Enable remote access to CouchDB by changing its binding address to 0.0.0.0
* Make sure port 5984 is open
* The relevant documentation for the above two steps is here: http://docs.couchdb.org/en/2.0.0/config/http.html
* Once installed, CouchDB's web interface (Futon) can be viewed here: http://[YOUR.IP.ADDR]:5984/_utils/
* After making your own account, Create a database called "app"
* Create a user called "sim" with password "2001robotdreams" for the Unity Simulator in the database. (As of right now, these credentials are coded into the Unity Simulator)
* Your CouchDB instance should now be able to receive HTTP requests from the Unity Simulator
* This is what the requests look like:
```
#!JSON

{
	"_id": "1018dc8189112087b81068757f5b1a73",
	"_rev": "1-b6346a7cc02ae46ce5b5dc17a4952ad0",
	"time": "4/28/2017 1:41:25 PM",
	"OS": "Linux 4.9 Fedora 24 64bit",
	"deviceType": "Desktop",
	"graphicsDeviceName": "Mesa DRI Intel(R) Haswell Mobile ",
	"graphicsDeviceVersion": "OpenGL 3.3 (Core Profile) Mesa 12.0.3",
	"processor": "Intel(R) Core(TM) i5-4200U CPU @ 1.60GHz",
	"processorCount": 4,
	"systemMemory": 5880,
	"sessionID": "FCWPLL8IMZ",
	"clientID": "5d247ed7a07b47e98f44a59c921ef926",
	"ipAddress": "209.147.144.26",
	"latitude": 33.414798736572266,
	"longitude": -111.9093017578125,
	"city": "Tempe",
	"state": "Arizona",
	"country": "United States",
	"coords": [{
			"x": 0,
			"y": 0,
			"z": 0
		},
		{
			"x": -2,
			"y": 0,
			"z": 0
		},
		{
			"x": -4,
			"y": 0,
			"z": 0
		},
		{
			"x": -5,
			"y": 0,
			"z": 0
		},
		{
			"x": -6,
			"y": 0,
			"z": 0
		},
		{
			"x": -7,
			"y": -1,
			"z": 0
		},
		{
			"x": -7,
			"y": -3,
			"z": 0
		},
		{
			"x": -7,
			"y": -5,
			"z": 0
		},
		{
			"x": -7,
			"y": -7,
			"z": 0
		},
		{
			"x": -4,
			"y": -7,
			"z": 0
		},
		{
			"x": -4,
			"y": -4,
			"z": 0
		},
		{
			"x": -4,
			"y": -2,
			"z": 0
		},
		{
			"x": -1,
			"y": -2,
			"z": 0
		},
		{
			"x": 0,
			"y": -3,
			"z": 0
		},
		{
			"x": 0,
			"y": -7,
			"z": 0
		},
		{
			"x": 0,
			"y": -9,
			"z": 0
		},
		{
			"x": 2,
			"y": -9,
			"z": 0
		},
		{
			"x": 3,
			"y": -9,
			"z": 0
		},
		{
			"x": 3,
			"y": -7,
			"z": 0
		},
		{
			"x": 3,
			"y": -5,
			"z": 0
		},
		{
			"x": 3,
			"y": -2,
			"z": 0
		},
		{
			"x": 4,
			"y": 0,
			"z": 0
		},
		{
			"x": 5,
			"y": -1,
			"z": 0
		},
		{
			"x": 5,
			"y": -5,
			"z": 0
		},
		{
			"x": 8,
			"y": -4,
			"z": 0
		},
		{
			"x": 8,
			"y": 1,
			"z": 0
		},
		{
			"x": 8,
			"y": 4,
			"z": 0
		},
		{
			"x": 6,
			"y": 6,
			"z": 0
		},
		{
			"x": 3,
			"y": 6,
			"z": 0
		},
		{
			"x": -1,
			"y": 6,
			"z": 0
		},
		{
			"x": -5,
			"y": 6,
			"z": 0
		},
		{
			"x": -9,
			"y": 6,
			"z": 0
		},
		{
			"x": -14,
			"y": 6,
			"z": 0
		}
	]
}
```


### Install This Application in CouchDB ###

* Install [couchapp](https://github.com/couchapp/couchapp)
* From the root of this repository, run 
```
#!code
couchapp push . http://[YOUR.IP.ADDR]:5984/app

```
* If you have authentication enabled, then it will look like this:

```
#!code
couchapp push . http://name:password@[YOUR.IP.ADDR]:5984/app
```
* The app will then be visible at this path:
```
#!code
http://[YOUR.IP.ADDR]:5984/app/_design/example/index.html
```

### Credit ###
* This application is based on a couchapp example from the couchapp team's repository that can be found here: https://github.com/couchapp/example